# tint2-deskobar
A tint2 sample configuration utilizing custom icons and executors for an informational panel without taskbar.

![Screenshot](screenshot.png)

## Usage

* copy `tint2rc` to `~/.config/tint2/tint2rc`
    * make a backup of your existing `~/.config/tint2/tint2rc` beforehand
* copy the folders `textures`, `scripts` and `icons` into `~/.config/tint2/`

### Note

**This is a sample config!** The `scripts/music` script is written for Audacious. Some commands in the tint2 executor definitions may not fit your system. Adjust values/commands as necessary.

## Requirements

* latest tint2 version
    * preferably at least [commit 4df8f475](https://gitlab.com/o9000/tint2/tree/4df8f475ce2ed8747c2696c8146f9a04582990f9) since that includes a fix for items moving around erronously which this config is affected by
* `lm_sensors` for CPU temperature reading (see `scripts/sensors`)
* `audacious` for audio player info and controls (or alter `scripts/music`)
* `gsimplecal` as on-click calendar for clock applet
* `wmctrl` for the virtual desktop/workspace number retrieval

### Recommended Software

* `pnmixer` or `volumeicon` for volume control from the system tray

## Disclaimer

Icons and their recolored variants are taken from the [Papirus icon set](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme/), used in accordance to GPLv3.